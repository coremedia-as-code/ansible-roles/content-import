import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/tmp/deployment_artefacts",
    "/etc/ansible/facts.d",
    "/opt/coremedia/content-management-server/server-lib",
    "/opt/coremedia/content-management-server/common-lib",
    "/opt/coremedia/content-management-server/current/webapps/content-management-server/WEB-INF/properties/corem",
    "/var/cache/coremedia/content-management-server/blobstore-assets"
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/content-management-server.fact",
    "/opt/coremedia/content-management-server/common-lib/coremedia-tomcat.jar",
    "/opt/coremedia/content-management-server/current/bin/setenv.sh",
    "/opt/coremedia/content-management-server/content-management-server.properties"
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.user("coremedia").exists
    assert host.user("content-management-server").exists
    assert host.group("coremedia").exists
